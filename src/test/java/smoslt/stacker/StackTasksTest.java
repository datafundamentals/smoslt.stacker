/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.stacker module, one of many modules that belongs to smoslt

 smoslt.stacker is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.stacker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.stacker in the file smoslt.stacker/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.stacker;

//import static org.junit.Assert.*;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import smoslt.domain.Schedule;
import smoslt.domain.ScheduleBuild;
import smoslt.given.ScoreTrackerImpl;
import smoslt.optionsapi.Options;

public class StackTasksTest {
	ScheduleBuild build = new BuildFixedRealisticSchedule();
	Schedule schedule = new Schedule(build);

	@Test
	public void testGo() {
		deleteDir();
//		new StackTasks().go(schedule, new Options());
		try{
			new StackTasks().go(new ScoreTrackerImpl(schedule),null);
			fail("never should have completed");
		}catch(IllegalStateException e){
			//expected
			
		}
//		throw new UnsupportedOperationException("fix me");
	}

	private void deleteDir(){
		try {
			FileUtils.deleteDirectory(new File("generated"));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
