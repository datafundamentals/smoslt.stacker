package smoslt.stacker;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import smoslt.domain.Task;
import smoslt.domain.testharness.TestHarness;

public class TaskResourcerTest {
	List<TaskResourcer> taskResourcers = new ArrayList<TaskResourcer>();
	TestHarness testHarness = new TestHarness();

	@Before
	public void setUp() throws Exception {
		for (Task task : testHarness.getTasks()) {
			taskResourcers.add(new TaskResourcer(task, testHarness
					.getScoreTracker(), testHarness.getSideEffects()));
		}
	}

	/*
	 * COMMENTS 150107 The sink-hole that is the TaskResourcer class and these
	 * tests are inadequate to each other, but once again I have run out of
	 * patience and time before all this is put away. So these tests are largely
	 * a joke, and little good other than being a way to launch the class into a
	 * parade of System.outs which must first be uncommented.
	 */

	@Test
	public void testExecute() {
		int i = 0;
		for (TaskResourcer taskResourcer : taskResourcers) {
			taskResourcer.execute();
			i = i + taskResourcer.startDay;
		}
		assertEquals(i, 7);
	}

	@Test
	public void testIntializeWithEarliestFeasibleStartDay() {
		for (TaskResourcer taskResourcer : taskResourcers) {
			taskResourcer.execute();
			assertEquals(2, taskResourcer.resourceReservers.keySet().size());
			for (long key : taskResourcer.resourceReservers.keySet()) {
				ResourceReserver resourceReserver = taskResourcer.resourceReservers
						.get(key);
				assertNotNull(resourceReserver);
				assertTrue(resourceReserver.getResource().getGroup()
						.equals("DevImpl"));
			}
		}
	}

}
