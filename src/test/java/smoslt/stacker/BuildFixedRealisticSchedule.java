/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.stacker module, one of many modules that belongs to smoslt

 smoslt.stacker is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.stacker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.stacker in the file smoslt.stacker/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.stacker;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import smoslt.domain.Resource;
import smoslt.domain.Task;
import smoslt.domain.ScheduleBuild;

/*
 * Unmaintainable spaghetti code creates bogus schedules for testing only. Not intended to be well written, etc. 
 */
public class BuildFixedRealisticSchedule implements ScheduleBuild {
	private List<Task> tasks;
	private int[] predecessors = { 2, 5, 6, 8, 9, 10, 12, 13, 14 };
	private Set<Resource> resources = new HashSet<Resource>();
	public static long TEST_RESOURCE_ID = (long)UUID.randomUUID().toString().hashCode();

	public static void main(String[] args) {
		BuildFixedRealisticSchedule build = new BuildFixedRealisticSchedule();
	}

	private void createTasks() {
		tasks = new ArrayList<Task>();
		// There are 5 Foos, 6 Bars, and 2 Wahs
		tasks.add(new Task(3, 0, "01", "8 Foo, William Tell"));
		tasks.add(new Task(10, 0,  "02", "48 Bar"));
		tasks.add(new Task(3, 0,  "03", "8 Bar"));
		tasks.add(new Task(8, 0,  "04", "24 Foo"));
		tasks.add(new Task(6, 0,  "05", "24 Foo, 2 Bar"));
		tasks.add(new Task(4, 0,  "06", "40 Foo"));
		tasks.add(new Task(7, 0,  "07", "8 Wah"));
		tasks.add(new Task(2, 0,  "08", "8 Foo"));
		tasks.add(new Task(5, 0,  "09", "8 Foo"));
		tasks.add(new Task(5, 0,  "10", "8 Foo"));
		tasks.add(new Task(3, 0,  "11", "8 Wah"));
		tasks.add(new Task(6, 0,  "12", "8 Wah, Mary Motherskin"));
		tasks.add(new Task(4, 0,  "13", "8 Foo, William Tell"));
		tasks.add(new Task(6, 0,  "14", "8 Bar"));
		tasks.add(new Task(2, 0,  "15", "8 Wah"));
	}

	public BuildFixedRealisticSchedule() {
		createTasks();
		addPredecessors();
		createResources();
	}

	@Override
	public List<Task> getTasks() {
		return tasks;
	}

	@Override
	public Set<Resource> getResources() {
		return resources;
	}

	private void createResources() {
		resources.add(new Resource( "William Tell", "Foo"));
		resources.add(new Resource( "Mary Motherskin", "Wah"));
		resources.add(new Resource( "Billy Bothers", "Foo"));
		resources.add(new Resource(BuildFixedRealisticSchedule.TEST_RESOURCE_ID, "Tommy Tunist", "Foo"));
		resources.add(new Resource( "Terry Teluver", "Bar"));
		resources.add(new Resource( "Sally Soliloquy", "Wah"));
		resources.add(new Resource( "Mantae Malea", "Bar"));
		resources.add(new Resource( "Becky Bontolix", "Bar"));
		resources.add(new Resource( "Bart Bontolix", "Bar"));
		resources.add(new Resource( "Bob Bontolix", "Bar"));
		resources.add(new Resource( "Betty Bontolix", "Bar"));
		resources.add(new Resource( "Tammy Tomface", "Foo"));
		resources.add(new Resource( "Martin Helover", "Foo"));
	}

	private void addPredecessors() {
		for (int i = 0; i < predecessors.length; i++) {
			tasks.get(predecessors[i]).addPredecessor(
					tasks.get(predecessors[i] - 1));
		}
		tasks.get(14).addPredecessor(tasks.get(6));
		tasks.get(6).addPredecessor(tasks.get(2));
	}

}
