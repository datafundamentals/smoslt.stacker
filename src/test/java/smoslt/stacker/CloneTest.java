/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.stacker module, one of many modules that belongs to smoslt

 smoslt.stacker is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.stacker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.stacker in the file smoslt.stacker/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.stacker;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import smoslt.domain.Schedule;
import smoslt.domain.ScheduleBuild;
import smoslt.domain.Task;
import smoslt.util.ObjectCloner;

public class CloneTest {
	ScheduleBuild build = new BuildFixedRealisticSchedule();
	Schedule schedule = new Schedule(build);

	@Test
	public void testGo() throws Exception{
		deleteDir();
		Schedule scheduleClone = (Schedule)ObjectCloner.deepCopy(schedule);
		assertEquals(scheduleClone.getTaskList().size(), schedule.getTaskList().size());
		assertEquals(scheduleClone.getResources().size(), schedule.getResources().size());
		assertEquals(scheduleClone.getResources().size(), 13);
		int i= 0;
		for(Task task:scheduleClone.getTaskList()){
			assertEquals(scheduleClone.getTaskList().get(i).getName(), task.getName());
			i++;
		}
	}

	private void deleteDir(){
		try {
			FileUtils.deleteDirectory(new File("generated"));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
