/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.stacker module, one of many modules that belongs to smoslt

 smoslt.stacker is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.stacker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.stacker in the file smoslt.stacker/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.stacker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.btrg.utils.random.RandomProvider;

import smoslt.domain.Schedule;
import smoslt.domain.ScoreTracker;
import smoslt.domain.SideEffect;
import smoslt.domain.Task;
import smoslt.given.ScoreTrackerImpl;
import smoslt.given.sideeffect.SideEffectFactory;
import smoslt.optionsapi.Option;
import smoslt.optionsapi.Options;

/**
 * Iterates through a schedule, one task at a time, by order provided in the
 * TeamTaskSet. Delegates the actual work of allocating resources to any given
 * task to the TaskResourcer.
 */
public class StackTasks {
	ScoreTracker scoreTracker;
	List<SideEffect> sideEffects;

	public List<Integer> go(ScoreTracker scoreTracker, Options options) {
		this.scoreTracker = scoreTracker;
		populateSideEffects(options);
		if (!hasOrGroupMultiplesSelected()) {
			walkTeamTaskSetInSequence();
//			scores.add(0);
//			scores.add(schedule.getCosts());
//			scores.add(RandomProvider.random0toMax(9));
//			scores.add(RandomProvider.random0toMax(9));
//			scores.add(RandomProvider.random0toMax(9));
//			scores.add(RandomProvider.random0toMax(9));
//			scores.add(RandomProvider.random0toMax(9));
//			scores.add(RandomProvider.random0toMax(9));
			printStartDays();
		} else {
//			schedule.setFail()
//			scores.add(-1);
//			scores.add(0);
//			scores.add(0);
//			scores.add(0);
//			scores.add(0);
//			scores.add(0);
//			scores.add(0);
//			scores.add(0);
		}
		return scoreTracker.getScores();
	}

	boolean noOpDueToOrGroupConfiguration() {
		boolean noOp = false;
		for (SideEffect sideEffect : sideEffects) {
			if (sideEffect.isOrGroupTitle()) {
				noOp = true;
				break;
			}
		}
		if (!noOp && !hasOrGroupMultiplesSelected()) {
			noOp = true;
		}
		return noOp;
	}

	boolean hasOrGroupMultiplesSelected() {
		Map<String, Integer> orGroupSizes = new HashMap<String, Integer>();
		for (SideEffect sideEffect : sideEffects) {
			String orGroupKey = sideEffect.getGroupKey();
			if (null != orGroupKey
					&& orGroupSizes.keySet().contains(orGroupKey)) {
				orGroupSizes.put(orGroupKey, orGroupSizes.get(orGroupKey) + 1);
			} else if (null != orGroupKey) {
				orGroupSizes.put(orGroupKey, 1);
			}
		}
		boolean returnValue = false;
		for (String key : orGroupSizes.keySet()) {
			int count = orGroupSizes.get(key);
			if (count > 1) {
				returnValue = true;
				break; // all it takes is one
			}
		}
		return returnValue;
	}

	private void printStartDays() {
		for (Task task : scoreTracker.getSchedule().getTaskList()) {
			System.out.print(" " + task.getStartDay());
		}
		System.out.println();
	}

	private void populateSideEffects(Options options) {
		sideEffects = new ArrayList<SideEffect>();
		if (null != options && null != options.getOptionList()) {
			for (Option option : options.getOptionList()) {
				if (option.isOnOff()) {
					sideEffects.add(SideEffectFactory.getInstance()
							.getSideEffect(option.getId()));
				}
			}
		}
	}

	private void walkTeamTaskSetInSequence() {
		for (long rootTaskId : scoreTracker.getSchedule().getTeamTaskSet().getRootTasksIds()) {
			doTeamTaskRun(rootTaskId);
		}
	}

	private void doTeamTaskRun(long rootTaskId) {
		for (Task task : scoreTracker.getSchedule().getTeamTaskSet().getTeamTaskList(rootTaskId)) {
			new TaskResourcer(task, scoreTracker, sideEffects).execute();
		}
	}

}
