/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.stacker module, one of many modules that belongs to smoslt

 smoslt.stacker is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.stacker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.stacker in the file smoslt.stacker/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.stacker;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;

import smoslt.domain.Resource;
import smoslt.domain.ResourceAvailability;
import smoslt.domain.ResourceTracker;
 

/**
 * See constructor - specific to one Resource, a specific firstDayRequired, and
 * a minimum number of days
 * 
 * Convenience class used to encapsulate most of the functions of creating and
 * interacting with a resource's list of ResourceAvailability objects.
 * 
 * Also see ResourceAvailability.
 */
public class ResourceAvailabilities {
	private List<ResourceAvailability> resourceAvailabilities;
	@SuppressWarnings("unused")
	private ResourceTracker resourceTracker;
	@SuppressWarnings("unused")
	private Resource resource;

	public ResourceAvailabilities(Resource resource, int firstDayRequired,
			int minimumNumberDays, ResourceTracker resourceTracker) {
		this.resourceTracker = resourceTracker;
		this.resource = resource;
		resourceAvailabilities = new ArrayList<ResourceAvailability>();
		ResourceAvailability resourceAvailability;
		int lastReservedDay = resourceTracker.getLastReservedDay(resource);
		int daysSlackAfterLast = firstDayRequired - lastReservedDay;
		/*
		 * if lastReservedDay<firstDayRequired, all I need to do is give it how
		 * many days preceeding that I will leave as a gap
		 */
		if (daysSlackAfterLast > 0) {
			resourceAvailability = new ResourceAvailability(resource,
					firstDayRequired, minimumNumberDays,
					daysSlackAfterLast - 1, ResourceAvailability.A_LOT);
			resourceAvailabilities.add(resourceAvailability);
		}

		/*
		 * if lastReservedDay>firstDayRequired (you are picking up a gap in the
		 * resource's schedule) and the difference is greater than the number of
		 * days required, then I need to look at the entire space between the
		 * two, and for each contiguous set of unreserved days that is long
		 * enough, create a ResourceAvailability
		 */
		else if (daysSlackAfterLast < 0
				&& daysSlackAfterLast + minimumNumberDays < 0) {
			int[] daysNotReserved = new int[Math.abs(daysSlackAfterLast)];
			Set<Integer> reservedDayStash = resourceTracker
					.getResourceUsedPerDay().get(resource.getId());
			int j = 0;
			for (int i = firstDayRequired; i < firstDayRequired
					+ Math.abs(daysSlackAfterLast); i++) {
				if (!reservedDayStash.contains(new Integer(i))) {
					daysNotReserved[j] = i;
					j++;
				}
			}
			SortedMap<Integer, Integer> candidateGaps = resourceTracker
					.durationsOfUnreservedDaysLargeEnough(daysNotReserved,
							minimumNumberDays);
			j = 0;
			for (int i : candidateGaps.keySet()) {
				if (j == 0) {
					resourceAvailability = new ResourceAvailability(resource,
							i, candidateGaps.get(i),
							resourceTracker
									.getUnreservedDaysImmediatelyPreceding(
											resource, i), candidateGaps.get(i)
									- minimumNumberDays);
					resourceAvailabilities.add(resourceAvailability);
				} else {
					resourceAvailability = new ResourceAvailability(resource,
							i, candidateGaps.get(i), 0, candidateGaps.get(i)
									- minimumNumberDays);
					resourceAvailabilities.add(resourceAvailability);
				}
				j++;
			}
			/*
			 * Now, add one more AFTER the last reserved day, because that is
			 * also still available
			 */
			resourceAvailability = new ResourceAvailability(resource,
					resourceTracker.getLastReservedDay(resource) + 1,
					minimumNumberDays, 0, ResourceAvailability.A_LOT);
			resourceAvailabilities.add(resourceAvailability);
		}
		/*
		 * if the lastReservedDay>firstDayRequired and the difference is less
		 * than the number of days required, or if there have never been any
		 * days reserved, then I just return the lastReservedDay+1 and I'm done
		 */
		else if ((daysSlackAfterLast < 0 && daysSlackAfterLast
				+ minimumNumberDays >= 0)
				|| lastReservedDay == 0) {
			resourceAvailability = new ResourceAvailability(resource,
					resourceTracker.getLastReservedDay(resource) + 1,
					minimumNumberDays, 0, ResourceAvailability.A_LOT);
			resourceAvailabilities.add(resourceAvailability);
		} else {
			throw new RuntimeException();// should have accounted for all
											// possibilities before it got here
		}
	}

	public int getResourceAvailabilityCount() {
		int count = 0;
		if (null != resourceAvailabilities) {
			count = resourceAvailabilities.size();
		}
		return count;
	}

	public ResourceAvailability getEarliestResourceAvailability() {
		ResourceAvailability resourceAvailability = null;
		if (null != resourceAvailabilities) {
			resourceAvailability = resourceAvailabilities.get(0);
		}
		return resourceAvailability;
	}

	public ResourceAvailability get(int index) {
		ResourceAvailability resourceAvailability = null;
		if (null != resourceAvailabilities) {
			resourceAvailability = resourceAvailabilities.get(index);
		}
		return resourceAvailability;
	}

	public List<ResourceAvailability> get() {
		return resourceAvailabilities;
	}

}
