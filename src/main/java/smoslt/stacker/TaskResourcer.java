/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.stacker module, one of many modules that belongs to smoslt

 smoslt.stacker is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.stacker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.stacker in the file smoslt.stacker/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.stacker;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import smoslt.domain.Resource;
import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreTracker;
import smoslt.domain.SideEffect;
import smoslt.domain.Task;

/**
 * This is a single use class that is as a place to gather the variables for a
 * task's latest startDays for resources, and then, according to some set of
 * rules, reserving the appropriate startDay for these resources and this task
 * into the ResourceTracker
 * 
 * First it sets the startDay, which it gets either from it's predecessor or if
 * there is none, sets to 1
 * 
 * Then, it always sets the specifiedResources first, before the group ones.
 * With each specified resource, the startDay is advanced to at minimum match
 * the needs of that specified resource.
 * 
 * Next, it sets the groupResources for each group specified. At this point it
 * does not know which of any group's resources are to be used, so it has to set
 * a ResourceReserver for all of the resources from that group, in order to able
 * to select from them on a subsequent pass. Also during this pass it must
 * re-adjust the start day upwards to at least the minimum that would be
 * required for all of these group members to be able to provide.
 * 
 * 
 */

/*
 * Other parts you might need to understand:
 * 
 * ResourceReserver is a class of fields that would be placed in the Resource
 * class, but did not wish to clutter resource class up with a bunch of junk, so
 * you should find this a one to one companion with each resource
 * 
 * ResourceAvailability is explained further in it's own class and needs to be
 * understood before this class is understood
 */

/*
 * CODE AUDIT NOTES 150106 This one class is the heart-beat of the entire
 * Stacker workflow. There are almost no unit tests and 18 separate functions
 * that have to fire in sequence, with almost 20 different if statements
 * governing the flow. Many of the if statements are not even critical but
 * instead are designed only to keep the cpu from churning needlessly. And there
 * is one while loop that runs in an infinite loop if something gets missed in
 * one of the if conditions designed to keep the cpu from churning. All totaled,
 * this is a witches brew of stupid programming almost guaranteed to fail
 * somewhere. The biggest challenge to implementing the unit tests is testing
 * all of the whacked out if statements that are designed to keep the cpu from
 * churning, and making sure that instead there are operations that are just not
 * happening instead. Some of the names for some of the fields/methods
 * themselves are somewhat confusing and this does not help things.
 * 
 * MORE NOTES 150107 Every time I go into this I have to re-memorize all the
 * parts and figure out how it all works. This seems to take me days, even
 * though it's very simple after I get it all figured how. Must be an easier
 * way. Hell of it is, if this were a physical machine that I could get my eyes
 * on, I could figure it out and make it work almost instantly.
 */

public class TaskResourcer {
	Task task;
	ScoreTracker scoreTracker;
	List<SideEffect> sideEffects;
	/*
	 * resourceReservers is a map of ResourceReservers for a specific task. The
	 * key is resource id. They are re-used over and over for each subsequent
	 * startDay's run
	 */
	Map<Long, ResourceReserver> resourceReservers = new HashMap<Long, ResourceReserver>();
	/*
	 * potentialGroupCountsThisStartDate is used to see if this day will even
	 * work for this group of resources. For example if there are 3 DevImpl
	 * resources required but this particular day only has 2 available with
	 * sufficient duration to finish the task, then we need to stop here,
	 * advance to the next day, and try again
	 */
	private Map<String, Integer> potentialGroupCountsThisStartDate = null;
	/*
	 * reservedGroupCountsThisStartDate is used to actually reserve a specific
	 * group of resources so that they can't be double committed and reserved
	 * for another task on that same day
	 */
	private Map<String, Integer> reservedGroupCountsThisStartDate = null;
	int startDay;
	boolean earliestStartDayValidatedAgainstResources;

	public TaskResourcer(Task task, ScoreTracker scoreTracker,
			List<SideEffect> sideEffects) {
		this.task = task;
		this.scoreTracker = scoreTracker;
		this.sideEffects = sideEffects;
		// org.btrg.dfb.print.PrintResourceTablulators.go(resourceReservers);
	}

	public void execute() {
		// System.out.println("SEQUENCE 0 execute");
		bailHireOrTakeLongerIfInadequateResources();
		executeSideEffects();
		intializeWithEarliestFeasibleStartDay();
		reserveResources();
	}

	void bailHireOrTakeLongerIfInadequateResources() {
		// System.out
		// .println("SEQUENCE 1 bailHireOrTakeLongerIfInadequateResources");
		for (String group : task.getGroupRequirements().keySet()) {
			/*
			 * must account for some total of group requirements and specified
			 * resources if in the same group, for each group. This programming
			 * still must be done. This should be complex set of operations,
			 * perhaps even executed in a different class.
			 * 
			 * The obvious default would be, given a task with 3 resources for 2
			 * days but only 2 resources in that group, then the task would
			 * instead be switched to 2 resources for 3 days
			 * 
			 * One of the key considerations is that Stacker is NOT for the
			 * purpose of perfecting a schedule, but for doing quick feasibility
			 * studies. So this routine would never hope to be perfected, but
			 * instead to help deal with some of the more typical types of
			 * curve-balls that it is thrown and allow the quick feasibility
			 * study to go on without errors
			 */
		}
	}

	void executeSideEffects() {
		// System.out.println("SEQUENCE 2 executeSideEffects");
		for (SideEffect sideEffect : sideEffects) {
			sideEffect.go(scoreTracker, task);
		}
	}

	void intializeWithEarliestFeasibleStartDay() {
		// System.out.println("SEQUENCE 3 intializeWithEarliestFeasibleStartDay");
		if (null != task.getSpecifiedResources()) {
			for (Resource resource : task.getSpecifiedResources()) {
				installUpToDateResource(resource);
			}
		}
		for (String groupName : task.getGroupRequirements().keySet()) {
			for (Resource resource : scoreTracker.getSchedule().getResources()) {
				if (resource.getGroup().equalsIgnoreCase(groupName)
						&& !resourceIsSpecifiedIndividually(resource)) {
					installUpToDateResource(resource);
				}
			}
		}
	}

	boolean resourceIsSpecifiedIndividually(Resource resource) {
		// System.out.println("SEQUENCE 4 resourceIsSpecifiedIndividually");
		boolean returnValue = false;
		if (null != task.getSpecifiedResources()) {
			for (Resource myResource : task.getSpecifiedResources()) {
				if (myResource.getId() == resource.getId()) {
					returnValue = true;
					break;
				}
			}
		}
		return returnValue;
	}

	void installUpToDateResource(Resource resource) {
		// System.out.println("SEQUENCE 5 installUpToDateResource");
		ResourceReserver resourceReserver = getFreshResourceReserver(resource);
		if (resourceReserver != null) {
			resourceReservers.put(resource.getId(), resourceReserver);
		}
	}

	/*
	 * 'fresh' in this instance means fresh resourceAvailabiltiies for this
	 * particular day. The resourceReserver itself is re-used consistently
	 * throughout the process
	 */
	ResourceReserver getFreshResourceReserver(Resource resource) {
		// System.out.println("SEQUENCE 6 getFreshResourceReserver");
		/*
		 * a new ResourceAvailabilities is always set, no matter what, because a
		 * new startDay is assumed, which reduces previous durationDay values.
		 * So previously adequate time durationDays are no longer adequate for
		 * some tasks
		 */
		ResourceAvailabilities resourceAvailabilities = new ResourceAvailabilities(
				resource, startDay, task.getDurationDays(), scoreTracker
						.getSchedule().getResourceTracker());
		ResourceReserver resourceReserver = resourceReservers.get(resource
				.getId());
		if (null == resourceReserver && !resource.isGroupSpecifierOnly()) {
			resourceReserver = new ResourceReserver(resource,
					resourceIsSpecifiedIndividually(resource),
					resourceAvailabilities);
			resourceReservers.put(resource.getId(), resourceReserver);
		} else if (null != resourceReserver) {
			resourceReserver.setResourceAvailabilities(resourceAvailabilities);
		}
		return resourceReserver;
	}

	void reserveResources() {
		// System.out.println("SEQUENCE 7 reserveResources");
		while (lacksEnoughResourcesForThisStartDate()) {
			startDay++;
			validateOrResetStartDate();
			if (startDay > 5000) {
				throw new IllegalStateException(
						"Failure to find enough resources to fulfill this requirement "
								+ task.getName()
								+ " please look to see if there is a mismatch "
								+ "between number of resources required, and number or "
								+ "resources provided. If there is no mismatch "
								+ "then there is a bug in this code");
			}
		}
		markResourcesAsReserved();
		reserveDates();
		task.setStartDay(startDay);
		// System.out.println("TASK - START DAY OF "+ startDay +
		// " WITH DURATION " +
		// task.getDurationDays() );
	}

	/*
	 * counts resources for each group that meetRequirements. If enough, it
	 * returns false, else if not enough, it returns true.
	 * 
	 * This does more than return true/false. It also changes state by
	 * increasingGroup counts
	 */
	boolean lacksEnoughResourcesForThisStartDate() {
		// System.out.println("SEQUENCE 8 lacksEnoughResourcesForThisStartDate");
		/*
		 * Always resets counters for every run
		 */
		initializePotentialGroupCountsThisStartDate();
		for (ResourceReserver reserver : resourceReservers.values()) {
			/*
			 * First, look at the group resources only just to make sure that
			 * the groupCounts are incremented
			 */
			if (!reserver.isSpecified()) {
				if (reserver.isPotential() && isYetToBeReserved(reserver)) {
//					System.out.println("\t\tINCREASING POTENTIAL GROUP COUNT "
//							+ reserver.getResource().getName()
//							+ " on startDay " + startDay);
					increasPotentialGroupCounts(reserver);
				}
			}
			// if (scoreTracker.getSchedule().getResourceTracker()
			// .isDayReserved(reserver.getResource(), startDay)) {
			// /*
			// * WTF this return makes no sense to me as of 150107 even though
			// * it seems to be working fine - why would the fact that a day
			// * is reserved for this resource automatically mean that it
			// * lacks enough resources for this day?
			// *
			// * Seems that this was fix put in at the last minute to solve
			// * another more complex bug, but instead this fix is just a bug
			// * in itself
			// */
			// System.out
			// .println("\t\tRETURNING lacksEnoughResourcesForThisStartDate "
			// + reserver.getResource().getName()
			// + " on startDay " + startDay);
			// return true;
			// }
		}
		boolean failsPerGroupResourceCount = failsPerGroupResourceCount();
		return failsPerGroupResourceCount;
	}

	private boolean isYetToBeReserved(ResourceReserver resourceReserver) {
		boolean isYetToBeReserved = true;
		if (scoreTracker.getSchedule().getResourceTracker()
				.isDayReserved(resourceReserver.getResource(), startDay)) {
			isYetToBeReserved = false;
		}
		return isYetToBeReserved;
	}

	void initializePotentialGroupCountsThisStartDate() {
		// System.out.println("SEQUENCE 9 initializePotentialGroupCountsThisStartDate");
		potentialGroupCountsThisStartDate = new HashMap<String, Integer>();
		for (String groupName : task.getGroupRequirements().keySet()) {
			potentialGroupCountsThisStartDate.put(groupName, 0);
		}
	}

	boolean failsPerGroupResourceCount() {
		// System.out.println("SEQUENCE 10 failsPerGroupResourceCount");
		boolean returnValue = false;
		for (String groupName : task.getGroupRequirements().keySet()) {
			if (potentialGroupCountsThisStartDate.get(groupName) < task
					.getGroupRequirements().get(groupName)) {
				returnValue = true;
				break;
			}
		}
//		System.out.println("\t\t\tGROUP FAIL " + returnValue + " on startDay "
//				+ startDay);
		return returnValue;
	}

	void validateOrResetStartDate() {
		// System.out.println("SEQUENCE 11 validateOrResetStartDate");
		/*
		 * sets each ResourceReserver as meetsRequirements or not, then if not,
		 * advances startDay by one day and quits
		 */
		for (ResourceReserver resourceReserver : resourceReservers.values()) {
			ResourceAvailabilities resourceAvailabilities = resourceReserver
					.getResourceAvailabilities();
//			System.out.println("\tCONSIDERING "
//					+ resourceReserver.getResource().getName()
//					+ " on startDay " + startDay);
			/*
			 * go through all resourceAvailabilities and find first startDate
			 * that works for this duration. Once you have that,
			 * ResourceReserver.setEarliestStartDayThatWorks() with that start
			 * day, and also if that resource is specified then also reset
			 * probableNextViableStartDay accordingly
			 */
			int firstAvailableDay = resourceAvailabilities
					.getEarliestResourceAvailability().getFirstDayAvailable();
//			System.out.println("\t\tFIRST AVAILABLE DAY "
//					+ resourceReserver.getResource().getName() + " on day "
//					+ firstAvailableDay);
			resourceReserver.setEarliestStartDayThatWorks(firstAvailableDay);
			if (firstAvailableDay <= startDay) {
				resourceReserver.setIsPotential(true);
			} else {
				resourceReserver.setIsPotential(false);
				/*
				 * Here it is checking if this is a specified resource, rather
				 * than one that is just from the group. If this one fails, all
				 * bets are off and you have to reset anyway
				 */
				if (resourceReserver.isSpecified()
						&& (firstAvailableDay > startDay)) {
					startDay = firstAvailableDay;
					resetAllResourceReserversNewStartDay();
					break;
				}
			}
		}
	}

	void increasPotentialGroupCounts(ResourceReserver reserver) {
		// System.out.println("SEQUENCE 12 increasPotentialGroupCounts");
		String groupName = reserver.getResource().getGroup();
		int count;
		if (potentialGroupCountsThisStartDate.containsKey(groupName)) {
			count = potentialGroupCountsThisStartDate.get(groupName) + 1;
		} else {
			count = 1;
		}
		potentialGroupCountsThisStartDate.put(groupName, count);
	}

	void markResourcesAsReserved() {
		// System.out.println("SEQUENCE 13 markResourcesAsReserved");
		/*
		 * Not sure exactly what all is involved but whatever is done, it's all
		 * done here. Main thing is not what to do with specified resources or
		 * groups with exactly the right number that meet requirements, because
		 * that is fairly much take them all. Harder thing is which ones not to
		 * choose when more resources are available than are required. Maybe a
		 * separate method for excluding resources not required?
		 */
		initializeReservedGroupCountsThisStartDate();
		for (ResourceReserver reserver : resourceReservers.values()) {
			if (reserver.isSpecified()) {
				reserver.setReservedForThisTask(true);
			} else if (!reserver.isSpecified() && reserver.isPotential()
					&& !reserver.isReservedForThisTask()
					&& isYetToBeReserved(reserver)
					&& quotaNotYetMet(reserver.getResource().getGroup())) {
				increaseFulfilledAndSetReserved(reserver);
			}
		}
	}

	void initializeReservedGroupCountsThisStartDate() {
		// System.out
		// .println("SEQUENCE 14 initializeReservedGroupCountsThisStartDate");
		reservedGroupCountsThisStartDate = new HashMap<String, Integer>();
		for (String groupName : task.getGroupRequirements().keySet()) {
			reservedGroupCountsThisStartDate.put(groupName, 0);
		}
	}

	boolean quotaNotYetMet(String groupName) {
		// System.out.println("SEQUENCE 15 quotaNotYetMet");
		int quota = task.getGroupRequirements().get(groupName);
		int reserved = reservedGroupCountsThisStartDate.get(groupName);
		if (reserved < quota) {
			return true;
		} else {
			return false;
		}
	}

	void increaseFulfilledAndSetReserved(ResourceReserver reserver) {
		// System.out.println("SEQUENCE 16 increaseFulfilledAndSetReserved");
		reserver.setReservedForThisTask(true);
		increaseFulfilled(reserver.getResource().getGroup());
	}

	void increaseFulfilled(String groupName) {
		// System.out.println("SEQUENCE 17 increaseFulfilled");
		int value = reservedGroupCountsThisStartDate.get(groupName);
		reservedGroupCountsThisStartDate.put(groupName, value + 1);
	}

	void resetAllResourceReserversNewStartDay() {
		// System.out.println("SEQUENCE resetAllResourceReserversNewStartDay");
		for (ResourceReserver reserver : resourceReservers.values()) {
			reserver.setIsPotential(false);
			reserver.setReservedForThisTask(false);
		}
	}

	void reserveDates() {
		// System.out.println("SEQUENCE 18 reserveDates");
		for (ResourceReserver reserver : resourceReservers.values()) {
			if (reserver.isReservedForThisTask()) {
//				System.out.println("RESOURCE "
//						+ reserver.getResource().getName() + " for task "
//						+ task.getName() + " on " + startDay);
				// System.out.println("DAY " + startDay + " TASK DURATION "
				// + task.getDurationDays());
				for (int i = startDay; i < (startDay + (task.getDurationDays())); i++) {
					scoreTracker
							.getSchedule()
							.getResourceTracker()
							.reserveDay(reserver.getResource(), i, task.getId());
				}
				scoreTracker.getSchedule().addAssignment(task.getId(),
						reserver.getResource().getId());
				scoreTracker
						.increment(ScoreKey.Hours, -task.getDurationHours());
			}
		}
	}

}
