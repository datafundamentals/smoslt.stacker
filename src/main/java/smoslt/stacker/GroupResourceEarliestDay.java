/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.stacker module, one of many modules that belongs to smoslt

 smoslt.stacker is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.stacker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.stacker in the file smoslt.stacker/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.stacker;

import java.util.SortedMap;
import java.util.TreeMap;

public class GroupResourceEarliestDay {
	private SortedMap<Integer, Integer> trackerMap = new TreeMap<Integer, Integer>();
	private int countRequired = 0;
	
	public GroupResourceEarliestDay(int countRequired){
		this.countRequired = countRequired;
	}
	
	public void addDay(int earliestDayAvailable) {
		if(trackerMap.containsKey(earliestDayAvailable)){
			trackerMap.put(earliestDayAvailable,  trackerMap.get(earliestDayAvailable)+1);
		}else{
			trackerMap.put(earliestDayAvailable, 1);
		}
	}
	
	public int getEarliestDay(){
		int earliestDay = 0;
		int count = 0;
		for(int n:trackerMap.keySet()){
			for(int i = 0 ;i<trackerMap.get(n);i++){
				count++;
				earliestDay = n;
			}
			if(count>=countRequired){
				break;
			}
		}
		return earliestDay;
	}

}
