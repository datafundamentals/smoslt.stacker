/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.stacker module, one of many modules that belongs to smoslt

 smoslt.stacker is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.stacker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.stacker in the file smoslt.stacker/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.stacker;

import smoslt.domain.Resource;


/**
 * When evaluating which group resources to reserve/commit to a specific task, the
 * program must first bring in all candidates and then choose the best one among
 * them. This set of candidate resources is maintained as a set of
 * ResourceReservers, which will then be chosen from. There is thus one set of
 * ResourceReservers created for each task, before committing specific resources
 * to a task.
 * 
 * ResourceReserver thus holds state of each resource while evaluating which
 * resources out of a group are the ideal resources to commit to a specific
 * task. The final decision on any specific resource is maintained in the
 * boolean reservedForThisTask field.
 * 
 * ResourceReserver also contains a reference to a ResourceAvailabilities
 * object, which is the first thing that is checked before determining whether a
 * specific resource is a candidate for a specific task
 */
public class ResourceReserver {
	private Resource resource;
	private boolean isSpecified;
	private boolean isReservedForThisTask;
	private boolean isPotential;
	private int earliestStartDayThatWorks;
	private ResourceAvailabilities resourceAvailabilities;

	public void setResourceAvailabilities(
			ResourceAvailabilities resourceAvailabilities) {
		this.resourceAvailabilities = resourceAvailabilities;
	}

	public ResourceReserver(Resource resource, boolean isSpecified,
			ResourceAvailabilities resourceAvailabilities) {
		this.resource = resource;
		this.isSpecified = isSpecified;
		this.resourceAvailabilities = resourceAvailabilities;
	}

	public Resource getResource() {
		return resource;
	}

	public void setReservedForThisTask(boolean isReservedForThisTask) {
		this.isReservedForThisTask = isReservedForThisTask;
	}

	public boolean isSpecified() {
		return isSpecified;
	}

	public boolean isReservedForThisTask() {
		return isReservedForThisTask;
	}

	public ResourceAvailabilities getResourceAvailabilities() {
		return resourceAvailabilities;
	}

	public boolean isPotential() {
		return isPotential;
	}

	public void setIsPotential(boolean isPotential) {
		this.isPotential = isPotential;
	}

	public int getEarliestStartDayThatWorks() {
		return earliestStartDayThatWorks;
	}

	public void setEarliestStartDayThatWorks(int earliestStartDayThatWorks) {
		this.earliestStartDayThatWorks = earliestStartDayThatWorks;
	}
}
